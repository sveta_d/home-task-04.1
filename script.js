function average(graph) {
    var sum = 0, totalQuantity = 0;
    return (function averageInside (graph){
        totalQuantity++;
        sum += graph.value;
        if (graph.children) {
            graph.children.forEach(function(item){
                averageInside (item);
            });
        }
        return sum / totalQuantity;
    })(graph);
}

function min(graph) {
    var minValue = graph;
    return (function minFunc (graph){
        if (graph.value < minValue.value) minValue = graph;
        if (graph.children) {
            graph.children.forEach(function(item){
                minFunc(item);
            });
        }
        return minValue;
    })(graph);
}

function max(graph) {
    var maxValue = graph;
    return (function maxFunc (graph){
        if (graph.value > maxValue.value) maxValue = graph;
        if (graph.children) {
            graph.children.forEach(function(item){
                maxFunc(item);
            });
        }
        return maxValue;
    })(graph);
}

console.log(average(graph));
console.log(min(graph));
console.log(max(graph));